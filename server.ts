require('dotenv').config();    //load environmental variables

import cloudSQL from "./src/database/cloudSQL/cloudSQL";
import {Song} from "./src/models/song";
import mongo from "./src/database/mongo";
import firestore from './src/database/firestore/firestore';
import app from "./src/app";

const port = parseInt(process.env.PORT!) || 3000;

cloudSQL
    .sync()
    .then(result => {
        return Song.findByPk('1440650719');
    })
    .then(song => {
        if (!song) {
            return Song.create({
                songID: '1440650719',
                name: 'Another One Bites the Dust',
                artist: 'Queen',
                album: 'The Platinum Collection (Greatest Hits I, II & III)',
                year: '1980',
                country: 'USA',
                genres: 'Rock',
                coverUri: "https://is1-ssl.mzstatic.com/image/thumb/Music128/v4/9e/58/3b/9e583b8c-785e-64ee-ce3f-dc365f263861/source/100x100bb.jpg"
            })
        }
        return song;
    })
    .then(song => {
        console.log(`Connected to cloudSQL!`);

        firestore.auth().listUsers().then(users => {
            console.log(users);
            console.log(`Connected to Firestore!`);
            mongo(app, port);
        })
    })
    .catch(err => console.log(err));

