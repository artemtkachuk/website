# Uttpic's website
![background](https://raw.githubusercontent.com/utter-picture/website/master/background.jpg)

Uttpic is a universal platform for social discovery of cultural heritage.

We are making an open-source product with a mission of empowering everyone to broaden their minds by providing the very precise representation of the world's current preferences in the world of arts and human creativity.  
