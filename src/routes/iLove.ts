import express from 'express';
import {getILove, postAddToILove, postSaveILove} from "../controllers/iLove";

const router = express.Router();

router.get('/i-love', getILove);
router.post('/save-i-love', postSaveILove);
router.post('/add-to-i-love', postAddToILove);

export default router;