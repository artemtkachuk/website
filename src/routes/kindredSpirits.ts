import express from 'express';
import {getKindredSpirits} from "../controllers/kindredSpirits";

const router = express.Router();

router.get('/kindred-spirits', getKindredSpirits);

export default router;