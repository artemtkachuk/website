import express from 'express';
import passport from 'passport';
import { authenticateOptions } from '../config/passport';
import * as authControllers from '../controllers/auth';
import {updateLastLogin} from "../util/updateLastLogin";

const router = express.Router();

router.post('/login', passport.authenticate('local', authenticateOptions), updateLastLogin);   //change last login and authorize
router.get('/login', authControllers.getLogin);
router.post('/logout',authControllers.postLogout);
router.get('/join', authControllers.getJoin);
router.post('/join', authControllers.postJoin);
router.get('/myprofile',  authControllers.redirectToProfile);

export default router;