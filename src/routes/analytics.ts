import express from 'express';
import { mailinglist } from "../util/mailingList";

let router = express.Router();

let path = process.env.COUNT_MAILING_LIST_PATH!;

router.get(path, mailinglist);

export default router;