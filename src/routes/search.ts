import express from 'express';
import * as searchControllers from '../controllers/search';
import {postAddToMyWishList} from "../controllers/wish-list";

const router = express.Router();

router.get('/search', searchControllers.getSearch);
router.post('/add-to-my-wish-list', postAddToMyWishList);

export default router;