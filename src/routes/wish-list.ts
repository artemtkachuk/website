import express from 'express';
const router = express.Router();

import * as wishListControllers from '../controllers/wish-list';

router.get('/wish-List', wishListControllers.getWishList);
router.post('/delete-from-wish-list', wishListControllers.postDeleteFromWishList);

export default router;