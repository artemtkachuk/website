import express from 'express';
import {postFollow, getProfile, postProfilePictureUpload} from '../controllers/userProfile';

const router = express.Router();

router.get('/profile/:userID', getProfile);
router.post('/follow/:userID', postFollow);
router.post('/profilePictureUpload', postProfilePictureUpload);

export default router;