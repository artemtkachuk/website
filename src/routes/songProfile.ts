import express from 'express';
import { getCreationProfile } from "../controllers/songProfile";

const router = express.Router();

router.get('/creation', getCreationProfile);

export default router;