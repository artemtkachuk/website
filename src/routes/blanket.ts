import express from 'express';

import { getBlanket, addToBlanket, postGiveStars } from "../controllers/blanket";

const router = express.Router();

router.get('/blanket', getBlanket);
router.post('/give-stars', postGiveStars);
router.post('/add-to-blanket', addToBlanket);

export default router;