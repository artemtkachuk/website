import {Router} from "express";
import {getBuildRating, getRenderFilters} from '../controllers/ratings';

const router = Router();

router.get('/ratings', getRenderFilters);
router.get('/build-rating', getBuildRating);

export default router;