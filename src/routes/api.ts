import express from 'express';
import {spotifyAskPermission} from "../api/spotify/askPermission";
import {getSpotifyCallback} from "../api/spotify/spotifyCallback";

const router = express.Router();

router.get('/spotifyLogin', spotifyAskPermission);
router.get('/spotifyCallback', getSpotifyCallback);

export default router;