import express from 'express';
import {getFollowees, getFollowers} from "../controllers/followers";

const router = express.Router();

router.get('/followers', getFollowers);
router.get('/followees', getFollowees);

export default router;