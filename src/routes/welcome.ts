import express from 'express';
import * as welcomeControllers from '../controllers/welcome';

let router = express.Router();

//routing
router.get('/', welcomeControllers.getIndex);
router.get('/index', welcomeControllers.getPrototype);
router.post('/stay-tuned', welcomeControllers.postStayTuned);
router.post('/contact',  welcomeControllers.postContact);

export default router;