import {User} from "../models/user";
import {Types} from "mongoose";
import {calculatePercentage} from "../functions/calculatePercentage";
import {neo4jUser} from "../models/neo4jUser";

//TODO make a cloud function
//TODO implement calculate triggers

// "users" is an array containing the Mongo documents with all users' data
// "user" is the Mongo user who made changes
// "currUser" is the Neo4j user who made changes
// "otherUser" is each Mongo user from "users"

export const recalculatePercentages = (userID: string) => {    //userID is the ID of a user who performed log #49 or log #56
    User
        .find( {})  //find all
        .then(users => {
            let user = users.find(user => user._id == userID); //mongo user
            users.splice(users.indexOf(user!),1);
            neo4jUser
                .find(user!._id.toString())
                .then(currNeodeUser => { //neo4j user
                    users.forEach(otherMongoUser => {    //mongo user
                        neo4jUser
                            .find(otherMongoUser._id.toString())
                            .then(otherNeodeUser => {
                                const newPercentage = calculatePercentage(user!, otherMongoUser);
                                currNeodeUser    //neo4j user
                                    .relateTo(otherNeodeUser, '%', {
                                        value: newPercentage
                                    })
                                    .then(res => {
                                        console.log(`The new % of match between userID ${user!._id} and userID ${otherMongoUser!._id} was updated to ${newPercentage} `);
                                    });
                            })
                        });
                    });
        })
}