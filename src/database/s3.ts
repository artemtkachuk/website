import * as AWS from 'aws-sdk';

const ID = process.env.AWS_S3_ID;
const SECRET = process.env.AWS_S3_SECRET;

const s3 = new AWS.S3({
    accessKeyId: ID,
    secretAccessKey: SECRET
})

export default s3;