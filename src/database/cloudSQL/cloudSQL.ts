import Sequelize from 'sequelize';

const env = process.env.ENV;
const dbName = process.env.CLOUD_SQL_DATABASE_NAME!;
const username = process.env.CLOUD_SQL_USERNAME!;
const password = process.env.CLOUD_SQL_PASSWORD!;
const instance = process.env.CLOUD_SQL_INSTANCE_NAME;

let dialectOptions = {};

if (env == `production`) {
    dialectOptions = {
        socketPath: `/cloudsql/${instance}`
    }
}

const cloudSQL = new Sequelize.Sequelize(dbName, username, password, {
    host: instance,
    dialect: 'mysql',
    dialectOptions: dialectOptions
});

export default cloudSQL;