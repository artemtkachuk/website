import * as admin from "firebase-admin";

const dbName = process.env.FIREBASE_DATABASE_NAME;

admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: `https://${dbName}.firebaseio.com`
});

export default admin;
