import mongoose from 'mongoose';
import {Application} from 'express';
import sequelize from "./cloudSQL/cloudSQL";

const mongoUrl = process.env.MONGO_URL;

let mongo = (app: Application, port: number) => {
    mongoose
        .connect(mongoUrl!, {useNewUrlParser: true, useUnifiedTopology: false, useCreateIndex: true }, (err) => {
            if (!err) {
                console.log('Connected to mongo!');
                app.listen(port);
                console.log(`App listens on port ${port}`);
            }
        })
        .catch(err => console.log(err));
}

export default mongo;