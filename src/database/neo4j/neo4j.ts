import Neode, {Schema} from 'neode';

const url = process.env.NEO4J_URL!;
const username = process.env.NEO4J_USERNAME!;
const password = process.env.NEO4J_PASSWORD!;

export const neo4j = new Neode(url, username, password);
export const builder = neo4j.query();