const ctx = document.getElementById('rose').getContext('2d');
ctx.canvas.width = 500;
ctx.canvas.height = 250;

const iLove = JSON.parse(`<%- JSON.stringify(iLove) %>`);
const genres = "<%= Object.keys(iLove) %>".split(',');

const values = genres.map(genre => {
    return iLove[genre.replace(/&amp;/g, '&')].length;
});
const maxVal = Math.max.apply(null, values);

const data = {
    labels: genres,
    datasets: [{
        data: values.map(value => (value / maxVal * 100).toFixed(0))
    }]
};

//TODO write function for properly displaying this. onResize()
const options  = {
    scale: {
        ticks: {
            beginAtZero: true,
            min: 0,
            max: 100,
            stepSize: 25
        }
    },
    legend: {
        display: false
    },
    responsive: false
}


const myRadarChart = new Chart(ctx, {
    type: 'radar',
    data: data,
    options: options
});