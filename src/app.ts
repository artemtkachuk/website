import express from 'express';

import { setViews } from "./middleware/setViews";
import { userBodyParsing } from "./middleware/useBodyParsing";
import { useAuthAndSessions } from "./middleware/useAuthAndSessions";
import { useRoutes } from "./middleware/useRoutes";
import { setStaticDir } from "./middleware/setStaticDir";

const app = express();

setViews(app);              //views
setStaticDir(app);          //static
userBodyParsing(app);       //parse body
useAuthAndSessions(app);    //auth
useRoutes(app);             //routes

export default app;

// TODO flash messages sometimes appear only when the page is reloaded! Fix it!


