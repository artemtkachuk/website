import codecs
import json

recordsFOLLOWS = open('recordsFOLLOWS.json', 'w')
recordsPERCENTAGE = open('recordsPERCENTAGE.json', 'w')

followsRecords = []
percentageRecords = []

with codecs.open('records.json', 'r', 'utf-8-sig') as jsonString:
    records = json.load(jsonString)
    for record in records:
        if record["p"]["segments"][0]["relationship"]["type"] == "FOLLOWS":
            followsRecords.append(record)
        else:
            percentageRecords.append(record)

json.dump(followsRecords, recordsFOLLOWS, indent=4)
json.dump(percentageRecords, recordsPERCENTAGE, indent=4)

recordsFOLLOWS.close()
recordsPERCENTAGE.close()