//FOLLOWS
CALL apoc.load.json("file:///recordsFOLLOWS.json") 
YIELD value
UNWIND value.p as p

MERGE (u1:User {userID: p.start.properties.userID})
MERGE (u2:User {userID: p.end.properties.userID})
MERGE (u1)-[r:FOLLOWS]->(u2)



//PERCENTAGE
CALL apoc.load.json("file:///recordsPERCENTAGE.json") 
YIELD value
UNWIND value.p as p

MERGE (u1:User {userID: p.start.properties.userID})
MERGE (u2:User {userID: p.end.properties.userID})
MERGE (u1)-[:PERCENTAGE {value: p.segments[0].relationship.properties.value}]->(u2)