import mongoose, {Schema, model, Types} from "mongoose";
import express from 'express';
import {ILog, MLog} from "../interfaces/log";
import {blanketSchema, wishlistSchema} from "./user";
import {IUser} from "../interfaces/user";
import {createLog} from "../logs/createLog";

export const requestPropertiesSchema = new Schema({
    userAgent: String,
    originalUrl: String,
    method: String,
    body: Object,
    params: Object,
    query: Object
});

const logSchema = new Schema({
    userID: Types.ObjectId,
    date: {
        type: Date,
        required: true
    },
    currWL: wishlistSchema,
    currBL: blanketSchema,
    currIL: {
        type: Schema.Types.Mixed,
    },   //TODO fix if possible,
    action: Object,
    code: {
        type: Number,
        required: true
    },
    id: String,
    stars: Number,
    req: {
        type: {requestPropertiesSchema},
        required: true
    }
}, { minimize: false });

logSchema.statics.createLog = createLog;

export const Log = model<ILog, MLog>('Log', logSchema, 'logs');