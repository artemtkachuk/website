import mongoose, { Schema, model } from 'mongoose';
import { IContact } from "../interfaces/contact";
//import { isEmail } from 'validator';

const notEmpty = (v: string) => {
    return v != "";
};

const contactSchema = new Schema({
   email: {
       type: String,
       required: true,
       lowercase: true,
       //validate: [isEmail, 'Invalid enmail']
   },

    name: {
        type: String,
        required: true,
        //validator: notEmpty
    },

    message: {
       type: String,
        required: true,
        //validator: notEmpty
    }
});

const Contact = model<IContact>('Contact', contactSchema, 'contact');

export default Contact;