import {neo4j} from "../database/neo4j/neo4j";

export const neo4jUser = neo4j.model('User', {
    userID: {
        primary: true,
        type: "string",
        required: true
    }
});

neo4j.model('User').relationship('%', 'relationship', 'PERCENTAGE', 'direction_both', 'User', {
    value: {
        type: 'number',
        required: true
    }
});

neo4j.model('User').relationship('follows', 'relationship', 'FOLLOWS', 'direction_out', 'User');
