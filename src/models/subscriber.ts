import mongoose, { Schema, model } from 'mongoose';
import mongooseUniqueValidator from "mongoose-unique-validator";
//import { isEmail } from 'validator';
import { ISubscriber, MSubscriber } from "../interfaces/subscriber";

const subscriberSchema = new Schema({
   email: {
       type: String,
       required: true,
       unique: true,
       lowercase: true,
       //validate: [ isEmail, 'Invalid email' ]
   }
});

subscriberSchema.statics.getSubscribers = function() {  //TODO test
    let subscribers: Array<String> = [];
    Subscriber.find((list, err) => {
            if (err) {
                throw err;
            } else {
                list.forEach((subscriber: ISubscriber) => {
                    subscribers.push(subscriber.email);
                })
            }
        })
        .catch(err => console.log(err));
    return subscribers.join(', ');
}
subscriberSchema.statics.countSubscribers = () => {
    return Subscriber.countDocuments({}, (err, count) => {
        console.log(`Number of people in the mailing list right now: ${count}`);
        return count;
    });
}

const flash = {
    message: 'The email is already in the database!'
};
//subscriberSchema.plugin(mongooseUniqueValidator, flash);


const Subscriber = model<ISubscriber, MSubscriber>('Subscriber', subscriberSchema,'mailing-list');
export default Subscriber;