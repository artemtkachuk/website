import Sequelize from 'sequelize';
import cloudSQL from "../database/cloudSQL/cloudSQL";
import { ISong, ISongStatic } from "../interfaces/song";

const songSchema = {
    songID: {
        type: Sequelize.STRING,
        required: true,
        unique: true,
        primaryKey: true,
        autoIncrement: false
    },
    name: {
        type: Sequelize.STRING,
        required: true
    },
    artist: {
        type: Sequelize.STRING,
        required: false
    },
    album: {
        type: Sequelize.STRING,
        required: false
    },
    year: {
        type: Sequelize.STRING,
        required: false
    },
    country: {
      type: Sequelize.STRING,
      required: false
    },
    genres: {
        type: Sequelize.STRING,
        required: false
    },
    coverUri: {
        type: Sequelize.STRING,
        required: false
    },
    url: {
        type: Sequelize.STRING,
        required: false
    }
};

export const Song = <ISongStatic>cloudSQL.define('Song', songSchema);