import {Request, Response} from "express";
import {Log} from "../models/log";
import {neo4j} from "../database/neo4j/neo4j";
import {User} from "../models/user";
import {createLog} from "../logs/createLog";

export const getFollowers = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req,71, undefined,{"followers": "Unauthorized. Redirected to /login"});
        res.redirect('/login');
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(me => {
            let username = req.query.username;
            // @ts-ignore
            User.find({username: username}).then(mongoUser => {  //TODO do not do this query if user === me
                let user = mongoUser[0];

                neo4j
                    // @ts-ignore
                    .cypher(`MATCH (:User { userID: '${user._id.toString()}' })<-[:FOLLOWS]-(User) RETURN User.userID as userID`, {})
                    .then((results) => {
                        // @ts-ignore
                        const usersFollowMe = results.records.map(user => user.toObject().userID);
                        User
                            .find()
                            .where('_id')
                            .in(usersFollowMe)
                            .exec()
                            .then(followers => {
                                // @ts-ignore
                                const mongoUsers = followers.filter(u => u._id.toString() != req.user._id.toString());
                                const mongoUsersIDs = mongoUsers.map(mu => { return "'" + mu._id + "'" });

                                neo4j.batch([
                                    // @ts-ignore
                                    {query: `MATCH (:User { userID: '${req.user._id}'})<-[r:PERCENTAGE]->(User) 
                                                    WHERE User.userID in [${mongoUsersIDs}]
                                                    RETURN User.userID as userID, r.value as value` },
                                    // @ts-ignore
                                    {query: `MATCH (:User {userID: '${req.user._id}'})-[:FOLLOWS]->(User) 
                                                    WHERE User.userID in [${mongoUsersIDs}]
                                                    RETURN User.userID as userID`}
                                ])
                                    .then(([p, f]) => {
                                        const percentages = p.records.map((record: { toObject: () => any; }) => record.toObject());
                                        const follow: Array<string> = f.records.map((record: { toObject: () => any; }) => record.toObject()).map((r: { userID: string; }) => r.userID);

                                        const followersWithData = followers.map(follower => {
                                            // @ts-ignore
                                            if (follower._id.toString() != req.user._id.toString()) {
                                                const value = percentages.find((p: { userID: { toString: () => any; }; }) => p.userID.toString() == follower._id.toString()).value;
                                                const iFollow = follow.includes(follower._id.toString());
                                                return {...follower.toJSON(), iFollow: iFollow, value: value, me: false};
                                            } else {
                                                return {...follower.toJSON(), iFollow: true, value: 100, me: true};
                                            }
                                        }).sort(({value: a}, {value: b}) => b - a);

                                        Log.createLog(req,  72, me!, {"followees": `Successfully rendered the followees of user ${user.username}`});

                                        res.render('User Profile/followers', {
                                            pUser: user,
                                            user: me,
                                            path: '/followers',
                                            followers: followersWithData
                                        })
                                    });
                            })
                    });
            });
        });
    }
}


export const getFollowees = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req,73, undefined,{"followers": "Unauthorized. Redirected to /login"});
        res.redirect('/login');
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(me => {
            let username = req.query.username;
            // @ts-ignore
            User.find({username: username}).then(mongoUser => {  //TODO do not do this query if user === me
                let user = mongoUser[0];

                neo4j
                    // @ts-ignore
                    .cypher(`MATCH (:User { userID: '${user._id.toString()}' })-[:FOLLOWS]->(User) RETURN User.userID as userID`, {})
                    .then((results) => {
                        // @ts-ignore
                        const usersIFollow = results.records.map(user => user.toObject().userID);
                        User
                            .find()
                            .where('_id')
                            .in(usersIFollow)
                            .exec()
                            .then(followees => {

                                // @ts-ignore
                                const mongoUsers = followees.filter(u => u._id.toString() != req.user._id.toString());
                                const mongoUsersIDs = mongoUsers.map(mu => { return "'" + mu._id + "'" });

                                neo4j.batch([
                                    // @ts-ignore
                                    {query: `MATCH (:User { userID: '${req.user._id}'})<-[r:PERCENTAGE]->(User) 
                                                    WHERE User.userID in [${mongoUsersIDs}]
                                                    RETURN User.userID as userID, r.value as value` },
                                    // @ts-ignore
                                    {query: `MATCH (:User {userID: '${req.user._id}'})-[:FOLLOWS]->(User) 
                                                    WHERE User.userID in [${mongoUsersIDs}]
                                                    RETURN User.userID as userID`}
                                ])
                                    .then(([p, f]) => {
                                        const percentages = p.records.map((record: { toObject: () => any; }) => record.toObject());
                                        const follow: Array<string> = f.records.map((record: { toObject: () => any; }) => record.toObject()).map((r: { userID: string; }) => r.userID);

                                        const followeesWithData = followees.map(followees => {
                                            // @ts-ignore
                                            if (followees._id.toString() != req.user._id.toString()) {
                                                const value = percentages.find((p: { userID: { toString: () => any; }; }) => p.userID.toString() == followees._id.toString()).value;
                                                const iFollow = follow.includes(followees._id.toString());
                                                return {
                                                    ...followees.toJSON(),
                                                    iFollow: iFollow,
                                                    value: value,
                                                    me: false
                                                };
                                            } else {
                                                return {...followees.toJSON(), iFollow: true, value: 100, me: true};
                                            }
                                        }).sort(({value: a}, {value: b}) => b - a);

                                        Log.createLog(req,  74, me!, {"followees": `Successfully rendered the followees of user ${user.username}`});

                                        res.render('User Profile/followees', {
                                            pUser: user,
                                            user: me,
                                            path: '/followees',
                                            followees: followeesWithData
                                        })
                                    });
                            })
                    });
            });
        });
    }
}


