import {Request, Response} from 'express';
import {createLog} from "../logs/createLog";
import {Log} from "../models/log";
import {User} from '../models/user';
import {neo4j} from "../database/neo4j/neo4j";
import {cloudSQLLookup} from "../api/iTunes/iTunes";
import _ from 'underscore';
import {isInILove} from "../util/isInILove";

export const getNews = (req: Request, res: Response) => {   //TODO @username started following you
    if (!req.user) {
        res.redirect('/login');
        Log.createLog(req, 65, undefined, {"news": "unauthorized. Redirected to login"});
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(me => {
            // @ts-ignore
            neo4j.cypher(`MATCH (:User { userID: '${req.user._id}' })-[:FOLLOWS]->(User) RETURN User.userID as userID`, {})
                .then((results) => {
                    const usersIFollow = results.records.map(user => {
                        // @ts-ignore
                        return user.toObject().userID;
                    });

                    Log.find({$or: [{code: 49}, {code: 56}]},
                        {code: 1, userID: 1, date: 1, action: 1, id: 1, stars: 1})
                        .where('userID')
                        .in(usersIFollow)
                        .sort({ date: 'desc' } )
                        .limit(200)
                        .exec()
                        .then(results => {
                            User.find().where('_id').in(results.map(result => result.userID)).exec()
                                .then(users => {
                                    cloudSQLLookup(results.map(result => result.id))
                                        .then(songs => {
                                            const news = results.map(result => {
                                                const user = users.find(mu => mu._id.toString() == result.userID.toString());
                                                const song = songs.find(s => s.songID == result.id);
                                                const resultingObj =  {
                                                    ...result.toJSON(),
                                                    isInBlanket: me!.blanket.some(song => song.songID === result.id),
                                                    username: user!.username,
                                                    metadata: song!
                                                };

                                                return {
                                                    ...resultingObj,
                                                    isInWishList: resultingObj.isInBlanket ? true : me!.wishList.includes(result.id)
                                                }
                                            });

                                            news.sort(({date: a}, {date: b}) => b - a);

                                            let newsByDate = _.chain(news)
                                                .groupBy((n) => {
                                                    return n.date?.toDateString();
                                                })
                                                // @ts-ignore
                                                .sort((a, b) => a - b)
                                                .toJSON();

                                            res.render('News/news', {
                                                path: '/news',
                                                user: req.user,
                                                n: newsByDate
                                            });
                                            // @ts-ignore
                                            createLog(req, 66, me!, {"news": "Success. Rendered news feed"});
                                        });
                                });
                        });
                });
        });
    }
}
