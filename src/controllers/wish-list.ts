import { Request, Response } from "express";
import { Song } from '../models/song';
import { User } from '../models/user';
import { Log } from "../models/log";
import {cloudSQLLookup, iTunesLookup} from "../api/iTunes/iTunes";

export const getWishList = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req, 32,undefined, {"getWL": "unauthorized. Redirected to /login"});
        res.redirect('/login');
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            cloudSQLLookup(user!.wishList).then(wishListSongs => {
                Log.createLog(req, 33, user!, {"getWL": "Success"});
                res.render('Wish List/wishList', {
                    user: req.user,
                    path: '/wish-list',
                    wishList: wishListSongs
                });
            })
        });
    }
}

export const postDeleteFromWishList = (req: Request, res: Response) => {    //TODO test
    if (!req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            Log.createLog(req, 34,user!, {"getWL": "unauthorized. Redirected to /login"});
            res.redirect('/login');
        });
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            if (req.body.songID && user!.wishList.includes(req.body.songID)) {
                user!.wishList.splice(user!.wishList.indexOf(req.body.songID), 1); //delete from the wish list
                user!
                    .save()
                    .then((user) => {
                        if (!user) {
                            console.log(`Error somewhere while trying to save user!`);
                            Log.createLog(req, 35,user!, {"error": `Can't save the changes made to wish list. Was supposed to delete id ${req.body.songID}`});
                        } else {
                            Log.createLog(req, 36, user!, {"delWL": `Deleted id ${req.body.songID} from WL. Success. Redirected to /wish-list`});
                            res.redirect('/wish-list');
                        }
                    })
                    .catch(err => console.log(err));
            } else {
                Log.createLog(req, 37, user!, {"delWL": `either id ${req.body.songID} is empty, or is it already in the WL. Redirected to /wish-list`});
                res.redirect('/wish-list');
            }
        });
    }
}

export const postAddToMyWishList = (req: Request, res: Response) => {
    if (!req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            Log.createLog(req, 38, user!, {"getWL": "unauthorized. Redirected to /login"});
            res.redirect('/login');
        });
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            if (!req.body.songID || user!.wishList.includes(req.body.songID)) {
                Log.createLog(req, 39, user!, {"addWL": `either id is empty, or it is already in the WL. Redirected back to /search?q=${req.body.query}`});
                res.redirect(`/search?q=${req.body.query}`);
            } else {
                iTunesLookup(Array.of(req.body.songID)).then(songs => {
                    let song = songs[0];
                    // @ts-ignore
                    song.genres = songs[0].genres;
                    Song
                        .findOrCreate({
                            where: {
                                songID: req.body.songID
                            },
                            defaults: song
                        })
                        .then(result => {
                            user!.wishList.push(req.body.songID);
                            user!.save().then((user) => {
                                if (!user) {
                                    console.log(`Error somewhere while trying to save user!`);
                                    Log.createLog(req, 40, user!, {"error": `id ${req.body.songID} could not be deleted from the WL. Details: ${user}`});
                                } else {
                                    Log.createLog(req, 41, user!, {"addWL": `added id ${req.body.songID} to WL.Redirected to ${req.body.fromSearch === "false" ? "/wish-list" : `/search?q=${req.body.query}`}`});
                                    if (req.body.fromNews) {
                                        res.redirect('/news');
                                    } else if (req.body.username) {
                                        res.redirect(`/profile/${req.body.username}`);
                                    } else {
                                        res.redirect(req.body.fromSearch === "false" ? `/wish-list` : `/search?q=${req.body.query}`);
                                    }
                                }
                            })
                        })
                })
            }
        });
    }
}
