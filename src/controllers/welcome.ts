import { Request, Response } from "express";
import Subscriber from "../models/subscriber";
import Contact from "../models/contact";
import {User} from "../models/user";
import { IContact } from "../interfaces/contact";
import { ISubscriber } from "../interfaces/subscriber";
import {Log} from "../models/log";

export const getIndex = (req: Request, res: Response) => {
    if (req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            Log.createLog(req, 1, user!, {"getIndex": "Redirected to /profile"});
            res.redirect('/myprofile');
        });
    } else {
        Log.createLog(req, 2, undefined, {"getIndex": "success"});
        res.render('Welcome/landing', {    //TODO configure flash
            failureFlash: req.flash('validationError'),
            successFlash: req.flash('subscribed'),
            contactError: req.flash('contactError'),
            contactSuccess: req.flash('contactSuccess')
        });
    }

};

export const getPrototype = (req: Request, res: Response) =>  {
    if (req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            Log.createLog(req, 3, user!, {"getPrototype": "Redirected to /profile"});
            res.redirect('/myprofile');
        });
    } else {
        Log.createLog(req, 4, undefined, {"getPrototype": "success"});
        res.render('Welcome/index', {
            path: '/index',
            user: null
        });
    }
};

export const postStayTuned = (req: Request, res: Response) => {   //TODO remove await
    if (req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            Log.createLog(req, 5, user!, {"stayTuned": "Redirected to /profile"});
            res.redirect('/myprofile');
        });
    } else {
        const data = {
            email: req.body['subscribe-email']
        };

        //TODO error handling here

        new Subscriber(data)
            .save()
            .then((subscriber: ISubscriber) => {
                console.log(`Subscribed!`);
                req.flash('subscribed', 'Thank you for signing up!');
                Log.createLog(req, 6, undefined, {"newSubscriber": `with email ${data.email}. Success`});   //TODO log email as a data field
                res.redirect('/');
            })
            .catch((err) => {
                console.log(`An error occured. Details:`);
                Log.createLog(req, 7,undefined, {"error": `in creating the subscriber. Details: ${err}`});
                console.log(err);
                req.flash('validationError', err);
                res.redirect('/');
            });
    }
}

export const postContact = (req: Request, res: Response) => { //TODO remove await
    if (req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            Log.createLog(req, 8, user!, {"contact": "Redirected to /profile"});
            res.redirect('/myprofile');
        });
    } else {
        const data = {
            name: req.body['name'],
            email: req.body['email'],
            message: req.body['message']
        };

        //TODO error handling here

        new Contact(data)
            .save()
            .then((contact: IContact) => {
                console.log(`The message is sent!`);
                Log.createLog(req, 9,undefined, {"newContact": `Name: ${data.name}, email: ${data.email}, message: ${data.message}. Success`});
                req.flash('contactSuccess', 'Thank you! Your message is sent!');
                res.redirect('/#contact');
            })
            .catch((err) => {
                console.log(`An error occured! Details:\n${err}`);
                Log.createLog(req, 10,undefined, {"error": `Unable to create new contact. Details: Name: ${data.name}, email: ${data.email}, message: ${data.message}. Message: ${err}`});
                req.flash('contactError', 'There is an error somewhere!');
            });

    }
}