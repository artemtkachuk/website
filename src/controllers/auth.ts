import { User, countries } from "../models/user";
import passport from "passport";
import hash from "../util/hashPassword";
import { Request, Response, NextFunction } from "express";
import { IUser } from "../interfaces/user";
import {Log} from "../models/log";
import {neo4j} from "../database/neo4j/neo4j";
import {recalculatePercentages} from "../triggers/recalculatePercentages";

export const getLogin = (req: Request, res: Response, next: NextFunction) => {
    if (req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            Log.createLog(req, 13, user!, {"login": "Redirected to /profile"});
            res.redirect('/myprofile');
        })
    } else {
        let message = req.flash('error');

        if (message.length > 0) {
            message = message[0];
        } else {
            message = null;
        }
        Log.createLog(req, 14,undefined,{"login": `Rendered /login with flash ${message}`});
        res.render('Welcome/login', {
            message: message,
            path: '/login',
            user: req.user || null
        });
    }
};



export const postLogout = (req: Request, res: Response, next: NextFunction) => {
    if (!req.user) {
        Log.createLog(req, 15, undefined, {"logout": "Redirected to /index"});
        res.redirect('/index');
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            req.logout();
            req.session!.destroy(err => {
                if (err) {
                    throw err;
                }
                Log.createLog(req, 16, user!, {"logout": "Redirected to /index"});
                res.redirect('/index');
            });
        })
    }
};

//TODO phone number and check phone and email
export const getJoin = (req: Request, res: Response, next: NextFunction) => {
    if (req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            Log.createLog(req, 17, user!, {"join": "Redirected to /profile"});
            res.redirect('/myprofile');
        })
    } else {
        Log.createLog(req, 18,undefined, {"join": "Rendered /join"});
        res.render('Welcome/join', {
            list: countries,
            path: '/join',
            user: req.user || null
        });
    }
};

export const postJoin = async (req: Request, res: Response, next: NextFunction) => {
    if (req.user) { //existing user goes to this route
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            Log.createLog(req, 19, user!, {"join": "Redirected to /profile"});
            res.redirect('/myprofile');
        });
    } else {

        // TODO validate data and redirect back if incorrect. Extra security layer!

        if (req.body.password != req.body.repeatPassword) {
            Log.createLog(req, 20, undefined, {"join": "Redirected to /join with flash"});
            res.redirect('/join');  // TODO req.flash('invalid')
        } else {
            delete req.body.repeatPassword;
            req.body.password = await hash(req.body.password);
            req.body.iLove = {};
            req.body.createdAt = new Date();
            req.body.lastLogin = new Date();

            new User(req.body)
                .save()
                .then(async (user: IUser) => {
                    await neo4j.create('User', {
                        userID: user._id.toString()
                    });
                    recalculatePercentages(user._id.toString());

                    req.login(user, (err) => {
                        if (err) {
                            throw err;
                        }
                        Log.createLog(req, 21, user!, {"join": "Successfully created new user, redirected to /profile"});
                        res.redirect('/myprofile'); //TODO record  on every entry
                    });
                })
                .catch(err => console.log(err));
            //TODO handle "User already in the Database"
        }
    }
};

export const redirectToProfile = (req: Request, res: Response) => {
    if (req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            res.redirect(`/profile/${user!.username}`);
        });
    } else {
        res.redirect('/login');
    }
}