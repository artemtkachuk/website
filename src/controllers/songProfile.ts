import { Request, Response } from 'express';
import { User } from "../models/user";
import { Log } from "../models/log";
import { iTunesLookup } from "../api/iTunes/iTunes";
import { isInILove, placeInILove } from "../util/isInILove";

export const getCreationProfile = (req: Request, res: Response) => {
    const songIdentifier = req.query.songID;

    if (req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            if (songIdentifier) {
                iTunesLookup(Array(songIdentifier.toString())).then(async songResult => {
                    const song = songResult[0];
                    // @ts-ignore

                    const isInBlanket = user!.blanket.some(blanketSong => blanketSong.songID === songIdentifier);
                    const stars = isInBlanket ? user!.blanket.find(({songID}) => songID === songIdentifier)!.stars : 0;
                    const isInWishList = user!.wishList.includes(songIdentifier);
                    const isInRatings = await isInILove(user!, songIdentifier);
                    const place = await placeInILove(user!, songIdentifier);

                    Log.createLog(req, 26, user!, {"creationProfile": `with id ${songIdentifier}. Success.`});
                    res.render('Song Profile/songProfile', {
                        track: {
                            ...song,
                            isInBlanket: isInBlanket,
                            stars: stars,
                            isInWishList: isInWishList,
                            isInILove: isInRatings,
                            place: place
                        },
                        user: req.user,
                        path: '/'
                    });
                })
            } else {
                Log.createLog(req, 27, user!, {"creationProfile": "empty query. Redirected to /index"});
                res.redirect('/index');
            }
        })
    } else {
        Log.createLog(req, 28,undefined, {"creationProfile": `with id ${songIdentifier}. Fail. Redirected back to /index`});
        res.redirect('/index');
    }
};