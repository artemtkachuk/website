import { Request, Response } from "express";
import { User } from "../models/user";
import { Song } from "../models/song";
import { Log } from "../models/log";
import {cloudSQLLookup, iTunesLookup} from "../api/iTunes/iTunes";
import { isInILove } from '../util/isInILove';
import {createLog} from "../logs/createLog";

export const getBlanket = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req, 42,undefined, {"getBL": "Unauthrorized. Redirected to /login"});
        res.redirect('/login');
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            // @ts-ignore
            const songIDs: Array<string> = user!.blanket.map(creation => creation.songID);
            cloudSQLLookup(songIDs).then(async blanketSongs => {
                // @ts-ignore
                const blanketSongsWithStars = await blanketSongs
                    .map(blanketSong => {
                        return {
                            ...blanketSong,
                            stars: user!.blanket.find(({songID}) => songID === blanketSong.songID)!.stars,
                            isInILove: isInILove(user!, blanketSong.songID)
                        }
                    })
                    .reverse();

                Log.createLog(req, 43, user!, {"getBL": "success"});

                res.render('Blanket/blanket',{
                    user: req.user,
                    path: '/blanket',
                    blanket: songIDs.length > 0 ? blanketSongsWithStars : []
                });
            });
        });
    }
}

export const postGiveStars = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req, 44, undefined, {"giveStars": "Unauthrorized. Redirected to /login"});
        res.redirect('/login');
    } else {
        const trackId = req.body.songID;
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            if (!trackId) {   //if there is an id attached to the query, then render. Otherwise redirect to WL
                Log.createLog(req, 45, user!, {"giveStars": "Fail. No trackID provided. Redirected to /wish-list"});
                res.redirect('/wish-list');
            } else {
                iTunesLookup(Array(trackId.toString())).then(song => {
                    Log.createLog(req, 46, user!, {"giveStars": `to id ${trackId}. Success.`});
                    res.render('Blanket/stars', {
                        path: '/blanket',
                        user: req.user,
                        track: song[0],
                        username: req.body.username
                    });
                });
            }
        });
    }
}

export const addToBlanket = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req, 47, undefined, {"addBL": "Unauthrorized. Redirected to /login"});
        res.redirect('/login');
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            const songID = req.body.songID;             //TODO no songId error handling Empty, undefined, etc...
            const stars = parseFloat(req.body.stars!);  //TODO no stars error handling. Empty, undefined, etc...

            if (!songID || !stars) {
                Log.createLog(req, 48, user!, {"addBL": "Fail. songID or stars are not provided. Redirected to /wish-List"});
                res.redirect('/wish-list');
            } else {
                iTunesLookup(Array.of(req.body.songID)).then(songs => {
                    let song = songs[0];
                    // @ts-ignore
                    song.genres = songs[0].genres;
                    Song
                        .findOrCreate({
                            where: {
                                songID: req.body.songID
                            },
                            defaults: song
                        })
                        .then(result => {
                            user!.blanket.push({songID: songID, stars: stars}); //add to user's Blanket
                            if (user!.wishList.find(song => song === songID)) {
                                user!.wishList.splice(user!.wishList.indexOf(songID), 1); //delete from the wish list
                            }

                            user!
                                .save()
                                .then(user => {
                                    if (!user) {
                                        console.log(`Error somewhere while trying to save user!`);
                                    } else {
                                        createLog(req, 49, user!, {"addBL": `pushed song id ${songID} with ${stars} stars. Success. Redirected to /blanket`}, songID, stars);
                                        if (req.body.username) {
                                            res.redirect(`/profile/${req.body.username}`);
                                        } else {
                                            res.redirect('/blanket');
                                        }
                                    }
                                })
                                .catch(err => console.log(err));
                        });
                });
            }
        });
    }
}