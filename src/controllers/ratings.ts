import {Request, Response} from "express";
import {Song} from "../models/song";
import {User} from "../models/user";
import {Log} from "../models/log";
import Sequelize from 'sequelize';
import {cloudSQLLookup} from "../api/iTunes/iTunes";
import {isInILove} from "../util/isInILove";

export const getRenderFilters = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req, 67, undefined, {"getFilters": `Unauthorized, redirected to /login`});
        res.redirect('/login');
    } else {
        //TODO continents
        //TODO store all cities that are currently in the db. Update upon registration

        //People
        let userSexs = User
            .find()
            .distinct('sex');

        let userYears = User
            .find()
            .distinct('dob');

        let userCountries = User
            .find()
            .distinct('country');

        //Creations
        let creationGenres = Song
            .findAll({
                raw: true,
                attributes: ['genres'],
                group: ['genres']
            });

        let creationYears = Song
            .findAll({
                raw: true,
                attributes: ['year'],
                group: ['year']
            });

        let creationCountries = Song
            .findAll({
                raw: true,
                attributes: ['country'],
                group: ['country']
            });


        Promise
            .all([creationGenres, creationYears, creationCountries, userSexs, userYears, userCountries])
            .then(records => {
                // @ts-ignore
                User.findById(req.user._id).then(user => {
                    Log.createLog(req, 68, user!, {"getFilters": `Unauthorized, redirected to /login`});
                });

                res.render('Ratings/filters', {
                    path: '/ratings',
                    user: req.user,

                    creationGenres: records[0].map(record => record.genres),
                    creationYears: records[1].map(record => record.year),
                    creationCountries: records[2].map(record => record.country),

                    userSexs: records[3],
                    userYears: [... new Set(records[4].map(date => new Date(date).getFullYear()))],
                    userCountries: records[5],
                })
        })
    }
}

export const getBuildRating = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req, 69, undefined, {"getBuildRating": `Unauthorized, redirected to /login`});
        res.redirect('/login');
    } else {
        // @ts-ignore
        User.findById(req.user._id)
            .then(me => {
                let filters = req.query;

                let getUsers = User
                    .find({
                        ...filters.userSex === "All" || filters.userSex === undefined ? {} : {sex: filters.userSex },
                        ...filters.userCountry === "All" || filters.userCountry === undefined ? {} : { country: filters.userCountry},
                        ...(filters.userYearGte === "All" || filters.userYearGte === undefined) && (filters.userYearLte === "All" || filters.userYearLte === undefined) ? {} :
                            {
                                dob: {
                                    ...filters.userYearGte === "All" || filters.userYearGte === undefined ? {} : { $gt: filters.userYearGte },
                                    ...filters.userYearLte === "All" || filters.userYearLte === undefined ? {} : { $lt: filters.userYearLte }
                                }
                            }
                    }, {
                        iLove: 1, _id: 0, dob: 1, firstName: 1, lastName: 1
                    })

                let getSongIDs = Song
                    .findAll({
                        where: {
                            ...filters.creationCountry === "All" || filters.creationCountry === undefined ? {} : { country: filters.creationCountry},
                            ...filters.creationGenre === "All" || filters.creationGenre === undefined ? {} : { genres: filters.creationGenre },
                            ...(filters.creationYearGte === "All" || filters.creationYearGte === undefined) && (filters.creationYearLte === "All" || filters.creationYearLte === undefined) ? {} :
                            {
                                year: {
                                    ...filters.creationYearGte === "All" || filters.creationYearGte === undefined ? {} : {[Sequelize.Op.gte]: parseInt(filters.creationYearGte)},
                                    ...filters.creationYearLte === "All" || filters.creationYearLte === undefined ? {} : {[Sequelize.Op.lte]: parseInt(filters.creationYearLte)}
                                }
                            }
                        },
                        attributes: ['songID', 'name'],
                        raw: true
                    })

                Promise
                    .all([getSongIDs, getUsers])
                    .then(async records => {
                        let songIDs = records[0].map(song => song.songID).slice(0, 200);        //TODO very important! Fix the boundary of 200 somehow. Call iTunes???
                        let users = records[1];

                        cloudSQLLookup(songIDs).then(iTunesSongs => {

                            //building the rating
                            let ratingSongs = iTunesSongs
                                .map(iTunesSong => {
                                    let rating = 0
                                    let met = false;
                                    users.forEach(user => {
                                        // @ts-ignore
                                        let iLoveGenre = user!.iLove[iTunesSong.genres];
                                        let pl = (iLoveGenre == undefined) ? -2 : iLoveGenre.indexOf(iTunesSong.songID);
                                        if (pl == -2) {
                                            // @ts-ignore
                                            let genreLength = Object.keys(user!.iLove).map(genre => user!.iLove[genre].length);

                                            rating += genreLength.length == 0 ? 0 : Math.max.apply(null, genreLength);
                                        } else if (pl == -1) {
                                            rating += iLoveGenre.length;
                                        } else {
                                            rating += pl;
                                            met = true;
                                        }
                                    });

                                    let ratingSong = {
                                        ...iTunesSong,
                                        rating: rating,
                                        isInILove: isInILove(me!, iTunesSong.songID),
                                        isInBlanket: me!.blanket.some(blanketSong => blanketSong.songID === iTunesSong.songID),
                                        isInWishList: false,
                                        met: met
                                    }
                                    ratingSong.isInWishList = ratingSong.isInBlanket ? true : me!.wishList.includes(ratingSong.songID);

                                    return ratingSong;

                                })
                                .sort(({rating: a}, {rating: b}) => a - b)
                                .filter(({met: a}) => a === true);

                            Log.createLog(req, 70, me!, {"getBuildRating": `Successfully built rating`});

                            res.render('Ratings/rating', {
                                user: req.user,
                                path: '/ratings',
                                rating: ratingSongs
                            });
                        });
                    })
            })
    }
}