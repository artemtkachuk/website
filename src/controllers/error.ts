import { Request, Response } from 'express';
import {User} from "../models/user";
import {Log} from "../models/log";

export const get404 = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req, 11,undefined, {"error": "404 redirected to /index"});
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            Log.createLog(req, 12, user!, {"error": "404 redirected to /index"});   //TODO see which pages people want most
        })
    }

    res.status(404).render('error/404',  {
        path: '/index',
        user: req.user || null
    });
};