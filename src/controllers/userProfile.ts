import fs from 'fs';
import formidable from 'formidable';
import { Request, Response } from 'express';
import {User} from "../models/user";
import {Song} from "../models/song";
import {Log} from "../models/log";
import {neo4j} from "../database/neo4j/neo4j";
import s3 from '../database/s3';
import {neo4jUser} from "../models/neo4jUser";
import {createLog} from "../logs/createLog";
import {cloudSQLLookup} from "../api/iTunes/iTunes";
import {isInILove} from "../util/isInILove";
import {SongType} from "../interfaces/song";

const BUCKET_NAME = process.env.AWS_S3_BUCKET_NAME;

export const getProfile = (req: Request, res: Response) => {
    if (req.user) {

        // @ts-ignore
        User.findById(req.user._id).then(me => {

            User.find({username: req.params.userID}).then(user => {

                const songIDs: Array<string> = user[0].blanket.map(creation => creation.songID);

                cloudSQLLookup(songIDs)
                    .then(blanketSongs => {

                        const blanketSongsWithInfo = blanketSongs
                            .map(blanketSong => ({
                                ...blanketSong,
                                stars: user[0].blanket.find(({songID}) => songID === blanketSong.songID)!.stars,
                                isInILove: isInILove(me!, blanketSong.songID),
                                isInBlanket: me!.blanket.some(song => song.songID === blanketSong.songID)
                            }))
                            .map(blanketSong => ({
                                ...blanketSong,
                                isInWishList: blanketSong.isInBlanket ? true : me!.wishList.includes(blanketSong.songID)
                            }))
                            .reverse();

                        const iLove = user[0].iLove;

                        if (Object.keys(iLove).length > 0) {    // { 'Hard Rock': ['123', '456'], 'Jazz': ['789', '101112'] }
                            for (let genre in iLove) {          // for each track get all info for rendering
                                let currGenreWithData: Array<SongType> = [];
                                // @ts-ignore
                                iLove[genre].forEach((songID: string) => {
                                   currGenreWithData.push(blanketSongs.find(({songID: a}) => a == songID)!);
                                });
                                // @ts-ignore
                                iLove[genre] = currGenreWithData;
                            }
                        }

                        neo4j
                            .batch([
                                {query: `MATCH (:User {userID: '${user[0]._id}'})-[:FOLLOWS]->(User) RETURN COUNT(User) as num;`},
                                {query: `MATCH (:User {userID: '${user[0]._id}'})<-[:FOLLOWS]-(User) RETURN COUNT(User) as num;`},
                            ])
                            .then(([nFollowees, nFollowers]) => {
                                let numFollowers = nFollowers.records[0].toObject().num;
                                let numFollowees = nFollowees.records[0].toObject().num;

                                // @ts-ignore
                                if (user[0]._id.toString() == req.user._id.toString()) {    //my profile
                                    res.render('User Profile/userProfile', {
                                        user: user[0],
                                        pUser: user[0],
                                        path: '/profile',
                                        anotherUser: false,
                                        iFollow: true,
                                        iLove: iLove,
                                        numFollowers: numFollowers,
                                        numFollowees: numFollowees,
                                        blanket: songIDs.length > 0 ? blanketSongsWithInfo : []
                                    });
                                    Log.createLog(req, 24, user[0]!, {"profile": `of user with _id = ${user[0]._id}. Success`});
                                } else {    //other user's profile
                                    // @ts-ignore

                                    neo4j.batch([
                                        // @ts-ignore
                                        {query: `MATCH (:User { userID: '${req.user._id}' })-[r:PERCENTAGE]-(:User {userID: '${user[0]._id}'}) RETURN r.value as value`},
                                        // @ts-ignore
                                        {query: `RETURN EXISTS( (:User {userID: '${req.user._id}'})-[:FOLLOWS]->(:User {userID: '${user[0]._id}'}) )`}
                                    ])
                                        .then(([{records}, follow]) => {
                                            // @ts-ignore
                                            User.findById(req.user._id).then(me => {
                                                Log.createLog(req, 24, me!, {"profile": `of user with _id = ${me!._id}. Success`});
                                                res.render('User Profile/userProfile', {
                                                    user: me,
                                                    pUser: user[0],
                                                    path: '/',
                                                    // @ts-ignore
                                                    anotherUser: true,
                                                    iFollow: follow.records[0]._fields[0],
                                                    // @ts-ignore
                                                    percentage: records[0].toObject().value.toFixed(3),
                                                    blanket: songIDs.length > 0 ? blanketSongsWithInfo : [],
                                                    iLove: iLove,
                                                    numFollowers: numFollowers,
                                                    numFollowees: numFollowees,
                                                });
                                            })
                                        });
                                }
                            });
                    });
            })
        })

    } else {
        Log.createLog(req, 25, undefined, {"profile": "unauthorized. Redirected to /login"});
        res.redirect('/login');
    }
};

export const postFollow = (req: Request, res: Response) => {
    if (!req.user) {
        createLog(req, 63, undefined, {"follow": "Unauthorized. Redirected to /login"});
        res.redirect('/login');
    }
    User.find({username: req.params.userID}).then(otherUser => {
        // @ts-ignore
        neo4jUser.find(req.user._id.toString()).then(user => {
            neo4jUser.find(otherUser[0]._id.toString()).then(otherNeo4jUser => {
                user
                    .relateTo(otherNeo4jUser, 'follows', {
                        since: new Date()
                    })
                    .then(result => {
                        // @ts-ignore
                        User.findById(req.user._id).then(user => {
                            // @ts-ignore
                            createLog(req, 64, user!, {"follow": `Success. User with id ${req.user._id} follows user with id ${otherUser[0]._id} now!`});
                        })
                        // @ts-ignore
                        console.log(`User with id ${req.user._id} follows user with id ${otherUser[0]._id} now!`);

                        //TODO refactor for just a redirect URI.

                        if (req.body.fromProfile) {
                            res.redirect(`/profile/${otherUser[0].username}`);
                        } else if (req.query.where == "fromSearch") {
                            res.redirect(`/search?q=${req.body.query}&u=true`);
                        } else if (req.query.redirectUrl) {
                            res.redirect(req.query.redirectUrl);
                        } else {
                            res.redirect('/kindred-spirits');
                        }
                    });
            })
        })
    })
};

export const postProfilePictureUpload = (req: Request, res: Response) => {
    if (!req.user){
        res.redirect('/login');
        //Log.createLog();
    } else {
        // @ts-ignore
        if (req.user.username != req.query.username) {
            res.redirect(`/profile/${req.query.username}`);
        } else {
            const form = new formidable.IncomingForm();
            // @ts-ignore
            let userID = req.query.username;

            form.parse(req, (err, fields, files) => {
                let oldpath = files.profilePicture.path;
                let path = oldpath + `.jpg`
                fs.rename(oldpath, path, (err) => {
                    if (err) {
                        throw err;
                    }

                    // Read content from the file
                    const fileContent = fs.readFileSync(path);

                    // Setting up S3 upload parameters
                    const params = {
                        Bucket: BUCKET_NAME!,
                        Key: userID + '.jpg',
                        Body: fileContent!,
                        ContentType: 'image/jpg'
                    };

                    // Uploading files to the bucket
                    s3
                        .upload(params, (err: any, data: { Location: any; }) => {
                            if (err) {
                                throw err;
                            }
                            console.log(`File uploaded successfully. ${data.Location}`);

                            User
                                // @ts-ignore
                                .findById(req.user._id)
                                .then(me => {
                                    // @ts-ignore
                                    me!.profilePictureURL = `${data.Location}`;

                                    me!
                                        .save()
                                        .then((user) => {
                                            if (!user) {
                                                throw err;
                                            }
                                            //delete file from system
                                            fs.unlink(path, (err) => {
                                                if (err) {
                                                    throw err;
                                                }
                                                console.log(`File deleted successfully!`);
                                                //Log.createLog();
                                                res.redirect('/myprofile');
                                            })
                                        })
                                })
                        })

                })
            })
        }
    }
};