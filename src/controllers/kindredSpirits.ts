import {Request, Response} from "express";
import {builder, neo4j} from "../database/neo4j/neo4j";
import {User} from "../models/user";
import {createLog} from "../logs/createLog";

export const getKindredSpirits = (req: Request, res: Response) => {
    if (!req.user) {
        res.redirect('/login');
        createLog(req, 61, undefined, {"ks": "unauthorized. Redirected to login"});
    } else {
        // builder
        //    .match('u', neo4jUser)
        //     // @ts-ignore
        //    .where('u.userID', req.user._id.toString())
        //    .relationship('PERCENTAGE', 'OUT', 'r', 1)
        //    .toAnything()
        //    .return('u', 'rel')
        //    .execute()
        // @ts-ignore
        //TODO convert to builder
        neo4j.cypher(`MATCH (:User { userID: '${req.user._id}' })<-[r:PERCENTAGE]->(User) RETURN User.userID as userID, r.value as value`, {})
             .then(({records}) => {
                // convert to convenient format, exclude 0% and sort in descending order
                 let kindredSpirits = records
                    .map(record => { return record.toObject() })
                     // @ts-ignore
                    .filter( ({value: v}) => v != 0 )
                     // @ts-ignore
                    .sort(({value: a}, {value: b}) => b - a );

                 //TODO convert to builder notation
                // @ts-ignore
                 neo4j.cypher(`MATCH (:User { userID: '${req.user._id}' })-[r:FOLLOWS]->(User) RETURN User.userID as userID`, {})
                      .then((results) => {
                          const iFollowUsers = results.records.map(user => {
                              // @ts-ignore
                              return user.toObject().userID;
                          });


                            // @ts-ignore
                          kindredSpirits = kindredSpirits.map(ks => ({...ks, iFollow: iFollowUsers.includes(ks.userID)}));

                          // @ts-ignore
                          User.find().where('_id').in(kindredSpirits.map(ks => ks.userID)).exec()
                              .then(mongoUsers => {
                                  kindredSpirits = kindredSpirits.map(ks => {
                                      // @ts-ignore
                                      const mongoUser = mongoUsers.find(mu => mu._id == ks.userID);
                                      return {...ks, username: mongoUser!.username, firstName: mongoUser!.firstName, lastName: mongoUser!.lastName, profilePictureURL: mongoUser!.profilePictureURL };
                                  });

                                  // @ts-ignore
                                  User.findById(req.user._id).then(user => {
                                      createLog(req, 62, user!, {"ks": "Rendered kindred spirits. Success."});
                                  });
                                  res.render('Kindred Spirits/kindredSpirits', {
                                      kindredSpirits: kindredSpirits,
                                      path: '/kindred-spirits',
                                      user: req.user
                                  })

                              })
                      })
             })
    }
}