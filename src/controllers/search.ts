import {Request, Response} from 'express';
import {iTunesSearch} from "../api/iTunes/iTunes";
import { SongType } from "../interfaces/song";
import { User } from '../models/user';
import { Log } from "../models/log";
import {neo4j} from "../database/neo4j/neo4j";

export const getSearch = (req: Request, res: Response) => { //TODO work on undefined cases separately

    let query = req.query.q;

    if (!req.user) {
        Log.createLog(req, 29,undefined, {"search": `unauthorized. query: ${query}. Redirected to /login`});
        res.redirect('/login');
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            if (query == "") {
                Log.createLog(req, 30, user!, {"search": "empty query"});
                res.render('Search/empty', {user: req.user, path: '/'});
            } else {
                //TODO url encoded
                iTunesSearch(query).then(iTunesresults => {
                    const regQuery = new RegExp(`${query.split(' ').join('|')}`, `i`);
                    User.find({
                        $or: [
                            {username: regQuery},
                            {firstName: regQuery},
                            {lastName: regQuery},
                            {city: regQuery},
                            {country: regQuery}
                        ]
                    }).limit(200)
                        .then(mongoU => {
                            // @ts-ignore
                            const mongoUsers = mongoU.filter(u => u._id.toString() != req.user._id.toString());
                            const mongoUsersIDs = mongoUsers.map(mu => { return "'" + mu._id + "'" });

                            neo4j.batch([
                                    // @ts-ignore
                                    {query: `MATCH (:User { userID: '${req.user._id}'})<-[r:PERCENTAGE]->(User) 
                                                    WHERE User.userID in [${mongoUsersIDs}]
                                                    RETURN User.userID as userID, r.value as value` },
                                    // @ts-ignore
                                    {query: `MATCH (:User {userID: '${req.user._id}'})-[:FOLLOWS]->(User) 
                                                    WHERE User.userID in [${mongoUsersIDs}]
                                                    RETURN User.userID as userID`}
                                ])
                                 .then(([p, f]) => {
                                     const percentages = p.records.map((record: { toObject: () => any; }) => record.toObject());
                                     const follow: Array<string> = f.records.map((record: { toObject: () => any; }) => record.toObject()).map((r: { userID: string; }) => r.userID);

                                     const users = mongoUsers.map(mongoUser => {
                                         // @ts-ignore
                                         const value = percentages.find(p => p.userID.toString() == mongoUser._id.toString()).value;
                                         const iFollow = follow.includes(mongoUser._id.toString());
                                         return {...mongoUser.toJSON(), iFollow: iFollow, value: value};
                                     })
                                     //TODO pass parameter u to the search function to open the user tab by default
                                     Log.createLog(req, 31, user!, {"search": query});
                                     res.render('Search/results', {
                                         iTunesResults: iTunesresults.map(result => ({
                                             ...result,
                                             isInBlanket: user!.blanket.some(song => song.songID === result.songID)
                                         })).map(result => ({
                                             ...result,
                                             isInWishList: result.isInBlanket ? true : user!.wishList.includes(result.songID)
                                         })),
                                         mongoUsers: users,
                                         query: query,
                                         user: req.user,
                                         path: '/search',
                                         u: req.query.u
                                     });
                                 });
                    });
                });
            }
        })
    }
}

