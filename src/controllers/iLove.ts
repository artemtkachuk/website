import { Request, Response } from "express";
import { User } from "../models/user";
import { Log } from "../models/log";
import {cloudSQLLookup, iTunesLookup} from "../api/iTunes/iTunes";
import { createLog } from "../logs/createLog";
import {SongType} from "../interfaces/song";

export const getILove = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req, 50,undefined, {"getIL": "unauthorized. Redirected to /login"});
        res.redirect('/login');
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(async (user) => {
            const iLove = user!.iLove;

            if (Object.keys(iLove).length > 0) {    // { 'Hard Rock': ['123', '456'], 'Jazz': ['789', '101112'] }
                for (let genre in iLove) {          // for each track get all info for rendering
                    // @ts-ignore
                    iLove[genre] = await cloudSQLLookup(iLove[genre]);
                    // @ts-ignore
                    iLove[genre] = iLove[genre]
                        .map((genreSong: SongType) => {
                            return {
                                ...genreSong,
                                stars: user!.blanket.find(({songID}) => songID === genreSong.songID)!.stars,
                            }
                        });
                }
            }   //TODO convert into functional language

            Log.createLog(req, 51, user!, {"getIL": "Success"});

            res.render('ILove/iLove', {
                iLove:  iLove,
                defaultGenre: req.query.genre || Object.keys(iLove)[0],
                path: '/i-love',
                user: req.user,
            });
        });
    }
}

export const postAddToILove = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req, 52,undefined, {"addIL": "Unauthorized. Redirected to /login"});
        res.redirect('/login');
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            if (!req.body.songID) {
                Log.createLog(req, 53, user!, {"addIL": "empty query. Redirected back to /blanket"});
                res.redirect('/blanket');
            } else {
                iTunesLookup(Array(req.body.songID)).then(song => {
                    const track = song[0];
                    const genre = track.genres;

                    // @ts-ignore
                    if ((genre in user!.iLove) && user!.iLove[genre].includes(track.songID)) {
                        Log.createLog(req, 54, user!, {"addIL": `id ${track.songID} already in IL. Redirecting to IL with the ${genre} genre tab`});
                        res.redirect(`/i-love?genre=${genre}`);
                    } else {
                        if (!(genre in user!.iLove)) {
                            // @ts-ignore
                            user!.iLove[genre] = Array(track.songID);
                        } else {
                            // @ts-ignore
                            user!.iLove[genre].push(track.songID);
                        }
                        user!.markModified('iLove');

                        user!.save().then(user => {
                            if (!user) {
                                Log.createLog(req, 55, user!, {"error": `cannot add id ${track.songID}. Details: ${user}`});
                                console.log(user);
                            }

                            createLog(req, 56, user!, {"addIL": `added id ${track.songID}. Success. Redirecting to /i-love?genre=${genre}`}, track.songID, undefined, genre);
                            res.redirect(`/i-love?genre=${genre}`);
                        })   //TODO изменения в ILOVE как записывать? Было-стало?
                    }
                })
            }
        });
    }
}

export const postSaveILove = (req: Request, res: Response) => {
    if (!req.user) {
        Log.createLog(req, 57,undefined, {"saveIL": "Unauthorized. Redirected to /login"});
        res.redirect('/login');
    } else {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            if (!req.body.iLove || (typeof req.body.iLove) != 'object') {          //TODO is empty, isNull etc
                Log.createLog(req, 58, user!, {"saveIL": "no IL provided. Redirected to /profile"});
                res.redirect('/myprofile');
            } else {
                // @ts-ignore
                user!.iLove = req.body.iLove;
                user!.markModified('iLove');
                user!.save().then(user => {
                    if (!user) {
                        Log.createLog(req, 59, user!, {"error": `Can't save new IL. Details: ${user}`});
                        console.log(user);
                    }
                    Log.createLog(req, 60, user!, {"saveIL": `Saved new IL. Sent 200 code. No redirect`});
                    res.status(200).end();
                })

            }
        });
    }
}