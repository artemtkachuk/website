import {IUser} from "../interfaces/user";

// This calculates the percentage only for ILove. Also need to make same one for Blanket when I remake it to {}

export const calculatePercentage = (user: IUser, otherUser: IUser) : Number => {
    const keysUser = Object.keys(user.iLove);
    const keysOtherUser = Object.keys(otherUser.iLove);

    const n = keysUser.length;       // # of genres of user
    const m = keysOtherUser.length;  // # of genres of otherUser

    if (n == 0 || m == 0) {
        return 0;
    } else {
        const intersection = keysUser.filter(genre => keysOtherUser.includes(genre));
        const l = intersection.length;

        const k = n + m - l;        //number of distinct genres in the ILoves of both users

        let percentage = 0;

        intersection.forEach(genre => {
            let L = 0.0, match = 0;
            // @ts-ignore
            const userILove = user!.iLove[genre];
            // @ts-ignore
            const otherUserILove = otherUser!.iLove[genre];

            userILove.forEach((track: string) => {
                if (otherUserILove.includes(track)) {
                    match += 1;
                    L += calculateL(userILove.indexOf(track), otherUserILove.indexOf(track));
                }
            })
            percentage += match * L / Math.pow(userILove.length + otherUserILove.length - match, 2);
        });

        return percentage * 100 / k;
    }
}

const calculateL = (place1: number, place2: number) => {
    return Math.min(place1 + 1, place2 + 1) / Math.max(place1 + 1, place2 + 1);
}