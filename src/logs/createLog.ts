import express from "express";
import {Log} from "../models/log";
import {IUser} from "../interfaces/user";
import {ILog} from "../interfaces/log";
import {recalculatePercentages} from "../triggers/recalculatePercentages";

export const createLog = async (req: express.Request, code: Number, user?: IUser, action?: Object, id?: String, stars?: Number, genre?: String) => {
    const newLog = {
        code: code,
        userID: user!._id || null,
        date: new Date(),
        currWL: user!.wishList || undefined,
        currBL: user!.blanket || undefined,
        currIL: user!.iLove || undefined,
        action: action || undefined,
        id: id || undefined,
        stars: stars || undefined,
        req: await constructReq(req)
    };
    const log = new Log(newLog);
    log.markModified('currIL');

    log.save()
        .then((log: ILog) => {
            console.log(`Sucessfull made a log.`);
            if (code === 49 || code === 56 || code == 60) {   //TODO implement and test here
                console.log('Now going to recalculate the % for the entire network.');
                recalculatePercentages(user!._id.toString());  //user is guaranteed to exist here
            }
        })
        .catch(err => console.log(err));
}

const constructReq = (req: express.Request) => {
    return {
        userAgent: req.header('user-agent'),
        originalUrl: req.originalUrl,
        method: req.method,
        body: req.body,
        params: req.params,
        query: req.query
    }
}