import mongoose, {Document, Types} from "mongoose";

export interface IUser extends Document {
    //properties
    username: string,
    password: string,
    email: string,
    firstName: string,
    lastName: string,
    dob: string,
    sex: string,
    country: string,
    city: string,
    wishList: Array<string>,
    blanket: Array<{songID: string, stars: Number}>,
    iLove: object,
    createdAt: string,
    lastLogin: string,
    profilePictureURL: string

    //methods
    validPassword(password: string): boolean
}