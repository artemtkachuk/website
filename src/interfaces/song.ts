import { Sequelize, Model, DataTypes, BuildOptions } from 'sequelize';

export interface ISong extends Model {
    songID: string
    name: string,
    artist: string,
    album: string,
    year: string,
    country: string,
    genres: string,
    coverUri: string,
    url: string
};

// Need to declare the static model so `findOne` etc. use correct types.
export type ISongStatic = typeof Model & {
    new (values?: object, options?: BuildOptions): ISong;
}

export type SongType = {
    songID: string
    name: string,
    artist: string,
    album: string,
    year: string,
    genres: string,
    coverUri: string,
    country: string,
    url: string
};
