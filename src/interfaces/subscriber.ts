import { Document, Model } from "mongoose";

export interface ISubscriber extends Document {
    email: string
}

export interface MSubscriber extends Model<ISubscriber> {
    countSubscribers(): number
}