import {Document, Model, Types} from "mongoose";
import express from 'express';
import {IUser} from "./user";

interface IReq {
    userAgent: String,
    originalUrl: String,
    method: String,
    body: Object,
    params: Object,
    query: Object
}

export interface ILog extends Document {
    userID: Types.ObjectId,
    date: Date,
    currWL: Types.Array<string>,
    currBL: Types.Array<string>,
    currIL: object,
    action: object,
    code: number,
    id: string,
    stars: number,
    req: IReq,

}

export interface MLog extends Model<ILog> {
    createLog(req: express.Request, code: Number, user?: IUser, action?: Object): void
}