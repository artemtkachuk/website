import {Application} from 'express';

export const setViews = (app: Application) => {
    app.set('view engine', 'ejs');
    app.engine('html', require('ejs').renderFile);
    app.set('views', './src/views');
}