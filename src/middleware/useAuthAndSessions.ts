//auth & sessions
import {Application} from 'express';
import session from "express-session";
import sessionOptions from "../config/session";
import passport from "passport";
import {deserializer, passportStrategyConfig, serializer} from "../config/passport";
import flash from "connect-flash";

export const useAuthAndSessions = (app: Application) => {
    app.use(session(sessionOptions));
    app.use(passport.initialize());
    passport.use(passportStrategyConfig);
    app.use(passport.session());
    app.use(flash());
    passport.serializeUser(serializer);
    passport.deserializeUser(deserializer);
};