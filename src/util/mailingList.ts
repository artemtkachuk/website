import { Request, Response } from 'express';
import Subscriber  from '../models/subscriber';
import {Log} from "../models/log";

export const mailinglist = async (req: Request, res: Response) => {
    let count = await Subscriber.countSubscribers();    //TODO error handling

    if (req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => Log.createLog(req, 22, user!, {"mailingList": "authorized. Sent data."}));
    } else {
        Log.createLog(req, 23, undefined, {"mailingList": "unauthorized. Sent data"});
    }

    res.send(`Number of people in the mailing list right now: ${count}`);
}
