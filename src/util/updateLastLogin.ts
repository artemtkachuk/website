import {Request, Response} from "express";
import {User} from "../models/user";

export const updateLastLogin = (req: Request, res: Response) => {   //TODO fix. Does not work
    if (req.user) {
        // @ts-ignore
        User.findById(req.user._id).then(user => {
            user!.lastLogin = new Date().toDateString();
            user!.save().then(user => {
                console.log(`Saved ${user}`);
            })
        });
    }
}