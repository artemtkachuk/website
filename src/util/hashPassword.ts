import bcrypt from 'bcrypt';

const saltRounds = 10;

const hash = (password: string) => {
    return bcrypt.hash(password, saltRounds);
};

export default hash;




