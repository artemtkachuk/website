import {IUser} from "../interfaces/user";

export const isInILove = (user: IUser, songID: string) => {
    for (var genre in user.iLove) {
        // @ts-ignore
        if (user!.iLove[genre].includes(songID)) {
            return true;
        }
    }
    return false;
}

export const placeInILove = (user: IUser, songID: string) => {  //TODO optimize for using isInILove return place
    for (var genre in user.iLove) {
        // @ts-ignore
        if (user.iLove[genre].includes(songID)) {
            // @ts-ignore
            return user.iLove[genre].indexOf(songID);
        }
    }
    return 0;
}