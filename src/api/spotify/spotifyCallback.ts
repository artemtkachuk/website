import { Request, Response } from "express";
import spotifyApi from "./wrapper";

export const getSpotifyCallback = (req: Request, res: Response) => {
    const code = req.query.code;

    spotifyApi.authorizationCodeGrant(code).then(
        (data) => {
            console.log('The token expires in ' + data.body['expires_in']);
            console.log('The access token is ' + data.body['access_token']);
            console.log('The refresh token is ' + data.body['refresh_token']);

            // Set the access token on the API object to use it in later calls
            spotifyApi.setAccessToken(data.body['access_token']);
            spotifyApi.setRefreshToken(data.body['refresh_token']);

           //TODO refresh token?
        },
        (err) => {
            console.log('Something went wrong!', err);
        }
    );
    res.redirect('/myprofile');
}