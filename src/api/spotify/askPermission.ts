import { Request, Response } from "express";
import spotifyApi from "./wrapper";

export const spotifyAskPermission = (req: Request, res: Response) => {
    var scopes = [
        'user-read-private',
        'user-read-email',
        'user-read-recently-played',
        'user-library-modify',
        'user-top-read',
        'user-library-read'
    ];

    const state = 'uttpicisthebestallever';

    const authorizeURL = spotifyApi.createAuthorizeURL(scopes, state);

    res.redirect(authorizeURL);
}