import { ItunesSearchOptions, searchItunes, ItunesLookupOptions, ItunesLookupType, lookupItunes, ItunesProperties, ItunesResult } from 'node-itunes-search';
import {Song} from "../../models/song";
import {SongType} from "../../interfaces/song";

export const iTunesSearch = (queryString: string) => {
    return searchItunes(new ItunesSearchOptions({ term: queryString })).then(data => { return createTrackObjects(data.results) });
}

export const iTunesLookup = (keys: Array<string>) => {
    return lookupItunes(new ItunesLookupOptions({ keys: keys, keyType: ItunesLookupType.ID })).then(data => { return createTrackObjects(data.results) });
}

export const cloudSQLLookup = async (songIDs: Array<string>) => {
    let data = await Song
        .findAll({
            where: {
                songID: songIDs
            },
            raw: true
        });

    return data.sort(({songID: a}, {songID: b}) => songIDs.indexOf(a) - songIDs.indexOf(b));
}

const createTrackObjects = (results: Array<ItunesProperties>): Array<SongType> => {
    const wishListSongs: Array<SongType> = [];

    results.forEach(track => {
        if (track.wrapperType === "track") {    //TODO add albums, artists and all other possible iTunes content
            wishListSongs.push({
                songID: track.trackId!.toString(),
                name: track.trackName!,
                artist: track.artistName!,
                album: track.collectionName!,
                year: track.releaseDate!.split('-')[0],
                genres: track.primaryGenreName!,
                coverUri: track.artworkUrl100!,
                country: track.country!,
                url: track.collectionViewUrl!
            })
        }
    });    //prepare the data

    return wishListSongs;
}


