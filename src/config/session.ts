import session from 'express-session';
import mongoose from 'mongoose';
import mongoDBSession from 'connect-mongodb-session';

const mongoSessionStorage = mongoDBSession(session);

const sessionOptions = {
    secret: process.env.SECRET!,
    resave: true,
    saveUninitialized: true,
    store: new mongoSessionStorage({
        uri: process.env.MONGO_URL!,
        databaseName: process.env.DATABASE_NAME!,
        collection: process.env.COLLECTION_NAME!
    })
    // cookie: {
    //   maxAge: 1000 * 60 * 60 * 24 * 7   //1 week
    // }
};

export default sessionOptions;