import { Request, Response } from "express";

import passportLocal from 'passport-local';
const LocalStrategy = passportLocal.Strategy;

import { User } from '../models/user';

const passportConfig = (username: string, password: string, done: any) => {
    User.findOne({username: username}, async (err, user) => {
            if (err) {
                return done(err, {message: 'Database error!'});
            }
            if (!user) {
                return done(null, false, {message: 'Incorrect username or password. Please try again!'});
            }
            const rightPass = await user.validPassword(password);
            if (!rightPass) {
                return done(null, false, {message: 'Incorrect username or password. Please try again!'});
            }
            return done(null, user);
        });
};

export const passportStrategyConfig = new LocalStrategy(passportConfig);
export const authenticateOptions = {
    failureRedirect: '/login',
    successRedirect: '/myprofile',
    failureFlash: true
};
export const serializer = (user: any, done: any) => {
    done(null, user.id);
}
export const deserializer = (id: any, done: any) => {
    User.findOne({_id : id}, (err, user) => {
        done(err, user);
    });
}

//TODO Custom callback
/*
The callback can use the arguments supplied to handle the auth result as desired. Note that when using a custom callback, it becomes the application's responsibility to establish a sessions (by calling req.login()) and send a response.

app.get('/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) { return next(err); }
    if (!user) { return res.redirect('/login'); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return res.redirect('/users/' + user.username);
    });
  })(req, res, next);
});
 */


// TODO Sign up and Login
 /*   Passport exposes a login() function on req (also aliased as logIn()) that can be used to establish a login sessions.

req.login(user, function(err) {
  if (err) { return next(err); }
  return res.redirect('/users/' + req.user.username);
});

When the login operation completes, user will be assigned to req.user.

Note: passport.authenticate() middleware invokes req.login() automatically. This function is primarily used when users sign up, during which req.login() can be invoked to automatically log in the newly registered user.
 */